class ArabicToEnglish
  # Regex for valid number
  NUMBER_FORMAT = /\A([+-])?(\d+)\z/

  # Grammar for number to name
  NUMBER_TO_NAME = {
    1_000_000_000 => "billion",
    1_000_000     => "million",
    1000          => "thousand",
    100           => "hundred",
    90            => "ninety",
    80            => "eighty",
    70            => "seventy",
    60            => "sixty",
    50            => "fifty",
    40            => "forty",
    30            => "thirty",
    20            => "twenty",
    19            => "nineteen",
    18            => "eighteen",
    17            => "seventeen",
    16            => "sixteen",
    15            => "fifteen",
    14            => "fourteen",
    13            => "thirteen",
    12            => "twelve",
    11            => "eleven",
    10            => "ten",
    9             => "nine",
    8             => "eight",
    7             => "seven",
    6             => "six",
    5             => "five",
    4             => "four",
    3             => "three",
    2             => "two",
    1             => "one"
  }

  def self.convert(int)
    # Check valid number
    return 'invalid number' unless int.to_s =~ NUMBER_FORMAT

    # Add minus if int is a negative number
    prefix = int.to_s.include?("-") ? "minus " : ""

    # Convert parameter to positive integer
    int = int.to_i.abs

    # Return zero if int equals 0
    if int == 0
      return "zero"
    else
      prefix + number_to_word(int)
    end
  end

  def self.number_to_word(int)
    # Initialize result
    str = ""

    # Iterate through NUMBER_TO_NAME then give out result using recursive
    NUMBER_TO_NAME.each do |num, name|
      if int == 0
        return str
      elsif int.to_s.length == 1 && int/num > 0
        return str + "#{name}"
      elsif int < 100 && int/num > 0
        return str + "#{name}" if int%num == 0
        return str + "#{name} " + number_to_word(int%num)
      elsif int/num > 0
        return str + number_to_word(int/num) + " #{name} " + number_to_word(int%num)
      end
    end
  end
end

ArabicToEnglish.convert(ARGV[0])
