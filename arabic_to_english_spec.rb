require "./arabic_to_english.rb"

RSpec.describe ArabicToEnglish do
  context 'pass invalid number' do
    it 'should return invalid number' do
      expect(ArabicToEnglish.convert('invalid123')).to eq("invalid number")
      expect(ArabicToEnglish.convert('--124')).to eq("invalid number")
      expect(ArabicToEnglish.convert('++8')).to eq("invalid number")
    end
  end

  context 'pass valid number' do
    context 'pass positive number' do
      it 'should return "zero" when pass 0' do
        expect(ArabicToEnglish.convert('0')).to eq("zero")
      end

      it 'should return "seven" when pass 7' do
        expect(ArabicToEnglish.convert('7')).to eq("seven")
      end

      it 'should return "fifteen" when pass 15' do
        expect(ArabicToEnglish.convert('15')).to eq("fifteen")
      end

      it 'should return "twenty seven" when pass 27' do
        expect(ArabicToEnglish.convert('27')).to eq("twenty seven")
      end

      it 'should return "one hundred seventeen" when pass 117' do
        expect(ArabicToEnglish.convert('117')).to eq("one hundred seventeen")
      end

      it 'should return "five thousand four hundred seventy one" when pass 5471' do
        expect(ArabicToEnglish.convert('5471')).to eq("five thousand four hundred seventy one")
      end

      it 'should return "one million eighty five" when pass 1000085' do
        expect(ArabicToEnglish.convert('1000085')).to eq("one million eighty five")
      end

      it 'should return "twenty one million ninety three thousand one" when pass 21093001' do
        expect(ArabicToEnglish.convert('21093001')).to eq("twenty one million ninety three thousand one")
      end
    end

    context 'pass negative number' do
      it 'should return "minus seven" when pass -7' do
        expect(ArabicToEnglish.convert('-7')).to eq("minus seven")
      end

      it 'should return "minus fifteen" when pass -15' do
        expect(ArabicToEnglish.convert('-15')).to eq("minus fifteen")
      end

      it 'should return "minus twenty one million ninety three thousand one" when pass -21093001' do
        expect(ArabicToEnglish.convert('-21093001')).to eq("minus twenty one million ninety three thousand one")
      end
    end
  end
end